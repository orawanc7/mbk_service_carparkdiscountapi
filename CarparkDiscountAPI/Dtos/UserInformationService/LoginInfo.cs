﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.UserInformationService
{
    public class LoginInfo
    {
        [XmlElement(ElementName = "LoginId")]
        public string LoginId { get; set; }

        [XmlElement(ElementName = "User_ID")]
        public string UserId { get; set; }

        [XmlElement(ElementName = "User_loginID")]
        public string UserLoginId { get; set; }
        
        [XmlElement(ElementName = "User_DomainName")]
        public string DomainName { get; set; }

        [XmlElement(ElementName = "User_NameTH")]
        public string UserNameTh { get; set; }

        [XmlElement(ElementName = "User_NameEN")]
        public string UserNameEn { get; set; }

        [XmlElement(ElementName = "User_Language")]
        public string Language { get; set; }

        [XmlElement(ElementName = "User_CompanyCode")]
        public string CompanyCode { get; set; }

        [XmlElement(ElementName = "User_CompanyNameTH")]
        public string CompanyNameTh { get; set; }

        [XmlElement(ElementName = "User_CompanyNameEN")]
        public string CompanyNameEn { get; set; }

        [XmlElement(ElementName = "User_Email")]
        public string Email { get; set; }

        [XmlElement(ElementName = "User_RegisterDate")]
        public string RegisterDate { get; set; }

        [XmlElement(ElementName = "User_Disable")]
        public bool Disable { get; set; }

        [XmlElement(ElementName = "User_StartWorkingDate")]
        public string StartWorkingDate { get; set; }

        [XmlElement(ElementName = "User_LastLoginDate")]
        public string LastLoginDate { get; set; }

        [XmlElement(ElementName = "User_Attempts")]
        public int Attempts { get; set; }

        [XmlElement(ElementName = "User_IsPrint")]
        public bool IsPrint { get; set; }

        [XmlElement(ElementName = "User_IsChangePassword")]
        public bool IsChangePassword { get; set; }

        [XmlElement(ElementName = "User_Status")]
        public string Status { get; set; }

        [XmlElement(ElementName = "IsSpecialGroup")]
        public bool IsSpecialGroup { get; set; }
    }
}                
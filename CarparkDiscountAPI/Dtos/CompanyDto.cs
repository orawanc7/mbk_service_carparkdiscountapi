﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class CompanyDto
    {        
        public string discount_app_token;
        public string discount_app_branch_id;
        public string discount_app_terminal_id;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos
{    
    public enum ErrorEnum
    {
        Error = -1,
        Success = 1,
        Timeout = 2
    }

    public class Error
    {
        [XmlElement(ElementName = "Code")]
        public string Code { get; set; }

        [XmlElement(ElementName = "MessageTH")]
        public string MessageTh { get; set; }

        [XmlElement(ElementName = "MessageEN")]
        public string MessageEn { get; set; }
    }
}
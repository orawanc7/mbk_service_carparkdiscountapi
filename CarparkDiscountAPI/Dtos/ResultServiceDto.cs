﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class ResultServiceDto   
    {
        public bool status { get; set; }
        public string error_msg { get; set; }        
    }
}
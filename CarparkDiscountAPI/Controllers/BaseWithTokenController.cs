﻿using CarparkDiscountAPI.Dtos;
using ITOS.Utility.Crypto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Controllers
{
    public class BaseWithTokenController : ApiController
    {
        public BaseWithTokenController()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
            culture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            culture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = culture;
        }

        protected string GetUrl(string urlApi,string url)
        {
            string strUrl = urlApi;

            
            if (!strUrl.Substring(strUrl.Length - 1).Equals("/"))
            {
                strUrl = strUrl + "/";
            }

            strUrl = strUrl + url;            
            
            return strUrl;
        }

    }
}
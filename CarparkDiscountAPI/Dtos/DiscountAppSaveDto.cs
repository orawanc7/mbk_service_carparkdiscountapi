﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountAppSaveDto
    {
        public decimal seq_id { get; set; }
        public string discount_id { get; set; }
        public decimal app_code { get; set; }

        public string public_flag { get; set; }
        public decimal campaign_id { get; set; }
        public string campaign_name { get; set; }
        public decimal expire_in { get; set; }
        public string privilege_message { get; set; }
        public decimal redeem_count { get; set; }
        public decimal item_number { get; set; }
        public decimal serial { get; set; }
        public string app_id { get; set; }
        public string use_date { get; set; }
        public decimal use_count { get; set; }
        public string user_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string id_card { get; set; }
        public string branch_id { get; set; }
        public string brand_id { get; set; }
        public string terminal_id { get; set; }
        public string merchant_id { get; set; }
        public decimal? point_per_unit { get; set; }
        public decimal? price_per_unit { get; set; }
        public decimal? original_price { get; set; }
        public decimal? total_points { get; set; }
        public decimal? discount_price { get; set; }
        public decimal? agency_id { get; set; }
        public decimal? location_agency_id { get; set; }
        public decimal? category_id { get; set; }
        public string partition_key { get; set; }
        public string row_key { get; set; }

        public string user { get; set; }
    }
}
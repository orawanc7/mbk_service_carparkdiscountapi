﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.ConnnectionService
{
    public class DBConnection
    {
        [XmlElement(ElementName = "CompCode")]
        public string CompCode { get; set; }

        [XmlElement(ElementName = "SysCode")]
        public string SysCode { get; set; }

        [XmlElement(ElementName = "DBMSType")]
        public string DBMSType { get; set; }

        [XmlElement(ElementName = "DBServer")]
        public string DBServer { get; set; }

        [XmlElement(ElementName = "DBPort")]
        public string DBPort { get; set; }

        [XmlElement(ElementName = "DBName")]
        public string DBName { get; set; }

        [XmlElement(ElementName = "UserName")]
        public string UserName { get; set; }

        [XmlElement(ElementName = "Pwd")]
        public string Pwd { get; set; }

        [XmlElement(ElementName = "ServiceName")]
        public string ServiceName { get; set; }

        [XmlElement(ElementName = "ConnectionString")]
        public string ConnectionString { get; set; }

        [XmlElement(ElementName = "DBCode")]
        public string DBCode { get; set; }        
    }
}                
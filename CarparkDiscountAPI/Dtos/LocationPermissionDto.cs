﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class LocationPermissionDto
    {
        public string CompanyCode { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string Url { get; set; }
        public string DiscountId { get; set; }
        public string DiscountDesc { get; set; }
        public bool HasEmp { get; set; }
        public bool HasApp { get; set; }
        public bool HasReceipt { get; set; }
        public bool HasOther { get; set; }
        public bool HasReceiptShop { get; set; }
    }
}
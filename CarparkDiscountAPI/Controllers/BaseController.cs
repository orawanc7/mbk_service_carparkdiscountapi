﻿using CarparkDiscountAPI.Dtos;
using ITOS.Utility.Crypto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Xml.Serialization;
using System.Configuration;
using System.Text;

namespace CarparkDiscountAPI.Controllers
{
    public class BaseController : ApiController
    {
        protected string SysCode {
            get {
                var sysCode = ConfigurationManager.AppSettings["SysCode"];

                if (sysCode == null)
                {
                    return "CARPARK1";
                }
                else
                {
                    return sysCode;
                }
            }
        }

        const string mSaltKey = "CARPARK";

        public BaseController()
        {
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
            culture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            culture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = culture;
        }

        protected string EncryptData(string data)
        {
            Crypto.EncryptionAlgorithm = ITOS.Utility.Crypto.Crypto.Algorithm.TripleDES;
            Crypto.Encoding = ITOS.Utility.Crypto.Crypto.EncodingType.BASE_64;
            return Crypto.EncryptFast(data);
        }

        protected string DecryptData(string data)
        {
            Crypto.EncryptionAlgorithm = ITOS.Utility.Crypto.Crypto.Algorithm.TripleDES;
            Crypto.Encoding = ITOS.Utility.Crypto.Crypto.EncodingType.BASE_64;
            return Crypto.DecryptFast(data);
        }

        protected ResultDto HasAuthorize(ref string loginId,ref string ipAddress)
        {
            ResultDto result = new ResultDto();

            var strLoginId = GetLogin();            
            var strIpAddress = GetIpAddress();

            var service = new mbk.sc.UserInfo.GetUserInformation();
            strLoginId = EncryptData(strLoginId);
            var resultService = service.GetInformation(strLoginId, this.SysCode, strIpAddress);

            using (var resultReader = new StringReader(resultService))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Dtos.UserInformationService.DsService));

                Dtos.UserInformationService.DsService resultConvert = (Dtos.UserInformationService.DsService)serializer.Deserialize(resultReader);

                /* MES9983 Session Expire */
                if (resultConvert.Error.Code.Equals("1"))
                {
                    result.Status = true;
                    result.Data = resultConvert.LoginInfo;
                } else 
                {
                    result.Status = false;
                    result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
                }
            }

            loginId = strLoginId;            
            ipAddress = strIpAddress;

            return result;
        }

        protected String GetLogin()
        {
            string strLoginId = String.Empty;            

            if (Request.Headers.Contains("LoginId"))
            {
                strLoginId = Request.Headers.GetValues("LoginId").First().ToString();                
            }
                        
            return strLoginId;
        }

        protected String GetCompCode()            
        {
            string strCompCode = String.Empty;

            if (Request.Headers.Contains("CompCode"))
            {
                strCompCode = Request.Headers.GetValues("CompCode").First().ToString();                
            }

            return strCompCode;
        }

        protected String GetIpAddress()
        {
            string strIpAddress = String.Empty;

            if (Request.Headers.Contains("IpAddress"))
            {
                strIpAddress = Request.Headers.GetValues("IpAddress").First().ToString();
            }

            return strIpAddress;
        }

        protected string GetUserPwd()
        {
            string strPasswordEncrypt = String.Empty;
            string strPasswordDecrypt = String.Empty;

            if (Request.Headers.Contains("UserPwd"))
            {
                strPasswordEncrypt = Request.Headers.GetValues("UserPwd").First().ToString();
                byte[] passwordEnctypt = System.Convert.FromBase64String(strPasswordEncrypt);

                byte[] salt = Encoding.ASCII.GetBytes(mSaltKey);                
                byte [] passwordDecrypt = Extensions.UtilExtension.AES_Decrypt(passwordEnctypt, salt);


                strPasswordDecrypt = Encoding.UTF8.GetString(passwordDecrypt);
            }

            return strPasswordDecrypt;
        }

        protected string GetUserId()
        {
            string strUserId = String.Empty;
            if (Request.Headers.Contains("UserId"))
            {
                strUserId = Request.Headers.GetValues("UserId").First().ToString();                
            }

            return strUserId;
        }

        protected string GetUrl (string url)
        {
            string strUrl = String.Empty;

            if (Request.Headers.Contains("Url"))
            {
                strUrl = Request.Headers.GetValues("Url").First().ToString();

                //strUrl = Crypto.DecryptFast(strUrl);

                if (!strUrl.Substring(strUrl.Length-1).Equals("/"))
                {
                    strUrl = strUrl + "/";
                }
                strUrl = strUrl + url;
            }

            //strUrl = "http://192.168.168.4/carparkservice_outdoor/api/" + url;

            return strUrl;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class ReceiptDto
    {
        public string ReceiptDiscountGroupId { get; set; }
        public string ReceiptDiscountGroupName { get; set; }
        public string ReceiptReceiptNo { get; set; }
        public Decimal? ReceiptReceiptAmt { get; set; }
    }
}
﻿using CarparkDiscountAPI.Dtos;
using ITOS.Utility.Crypto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Controllers
{
    public class AuthenticateController : BaseController
    {
        

        [HttpPost]
        [Route("api/Authenticate/Login")]
        public IHttpActionResult Login(LoginDto loginDto)
        {
            var result = new ResultDto();

            var service = new mbk.sc.LoginService.LoginService();
            var resultService = service.Login(this.SysCode, loginDto.UserId, loginDto.UserPwd, loginDto.IpAddr);
            //var resultService = service.Login(_sysCode, "mbk-center\\train01", "train01", loginDto.IpAddr);


            using (var resultReader = new StringReader(resultService))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Dtos.LoginService.DsService));

                Dtos.LoginService.DsService resultConvert = (Dtos.LoginService.DsService)serializer.Deserialize(resultReader);

                /* MES9983 Session Expire */
                if (resultConvert.Error.Code.Equals("MES9993"))
                {
                    
                    result.Status = true;
                    //resultConvert.LoginInfo.LoginId = EncryptData(resultConvert.LoginInfo.LoginId);
                    result.Data = resultConvert.LoginInfo;
                }
                else if (resultConvert.Error.Code.Equals("MES9958"))
                {
                    result.Status = false;
                    result.Message = "ไม่สามารถเข้าระบบได้ เนื่องจาก User หรือ Password ผิด";
                } else { 
                    result.Status = false;
                    result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
                }
            }
            

            /**
                <dsService>
                <Error>
                <Code>MES9993</Code>
                <MessageTH>เข้าระบบสำเร็จ</MessageTH>
                <MessageEN>Login success</MessageEN>
                </Error>
                <LoginInfo>
                <LoginId>15882</LoginId>
                <User_ID>178</User_ID>
                <User_LoginID>train01</User_LoginID>
                <User_EmployeeCode />
                <User_DomainName>mbk-center</User_DomainName>
                <User_NameTH>train01</User_NameTH>
                <User_NameEN>train01</User_NameEN>
                <User_Language>EN</User_Language>
                <User_CompanyCode>001</User_CompanyCode>
                <Company_NameTH>บริษัท เอ็ม บี เค จำกัด (มหาชน)   </Company_NameTH>
                <Company_NameEN>MBK Public Company Limited</Company_NameEN>
                <Branch_Code />
                <Branch_NameTH />
                <Branch_NameEN />
                <Dept_Code />
                <Dept_NameTH />
                <Dept_NameEN />
                <ipAddress />
                </LoginInfo>
                </dsService>
             */

                //var jsonResult = @"{
                //""Status"": true,
                //""Message"": null,
                //""Data"": {
                //    ""LoginId"": 15884,
                //    ""User_ID"": ""178"",
                //    ""User_loginID"": null,
                //    ""User_EmployeeCode"": """",
                //    ""User_DomainName"": ""mbk-center"",
                //    ""User_NameTH"": ""train01"",
                //    ""User_NameEN"": ""train01"",
                //    ""User_Language"": ""EN"",
                //    ""User_CompanyCode"": ""001"",
                //    ""User_CompanyNameTH"": null,
                //    ""User_CompanyNameEN"": null,
                //    ""Branch_Code"": """",
                //    ""Branch_NameTH"": """",
                //    ""Branch_NameEN"": "",
                //    ""Dept_Code"": """",
                //    ""Dept_NameTH"": """",
                //    ""Dept_NameEN"": """",
                //    ""IpAddress"": ""192.168.168.1""
                //}
                //}";

                return Json(result);
        }

        private void GetUserInformation(string loginId,string userId,string ipAddr)
        {
            var service = new mbk.sc.UserInfo.GetUserInformation();
            var resultService = service.GetUserInfo(loginId,userId, this.SysCode, ipAddr);
            using (var resultReader = new StringReader(resultService))
            {
                Console.Write("TEST");
            }
        }
        

        [HttpPost]
        [Route("api/Authenticate/Logout")]
        public IHttpActionResult Logout()
        {
            var result = new ResultDto();

            string strLoginId = String.Empty;
            string strCompCode = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                var service = new mbk.sc.LogoutService.LogoutService();
                var resultService = service.Logout(strLoginId, this.SysCode, strIpAddress);
                using (var resultReader = new StringReader(resultService))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Dtos.LogoutService.DsService));

                    Dtos.LogoutService.DsService resultConvert = (Dtos.LogoutService.DsService)serializer.Deserialize(resultReader);

                    if (resultConvert.Error.Code.Equals("MES9982"))
                    {
                        result.Status = true;
                    }
                    else
                    {
                        result.Status = false;
                        result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
                    }
                }                
            }
            /**
                <dsService>
                  <Error>
                    <Code>MES9982</Code>
                    <MessageTH>Logout Success</MessageTH>
                    <MessageEN>Logout Success</MessageEN>
                  </Error>
                </dsService>

                <dsService>
                  <Error>
                    <Code>MES9983</Code>
                    <MessageTH>Session Expire</MessageTH>
                    <MessageEN>Session Expire</MessageEN>
                  </Error>
                </dsService>
             */
           

            return Json(result);
        }
    }
}

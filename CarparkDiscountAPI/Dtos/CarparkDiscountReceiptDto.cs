﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class CarparkDiscountReceiptDto
    {
        public string SeqId { get; set; }
        public string DiscountId { get; set; }
        public string OldDiscountId { get; set; }
        public string PublicFlag { get; set; }
        public string ReceiptMode { get; set; }
        public List<ReceiptDto> Receipts { get; set; }
        public string UserId { get; set; }
    }
}
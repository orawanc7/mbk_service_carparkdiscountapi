﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class CarDto
    {
        public decimal seq_id { get; set; }
        public decimal? ref_seq_id { get; set; }
        public string car_lno { get; set; }
        public string prox_id { get; set; }
        public string ref_prox_id { get; set; }
        public string start_date_time { get; set; }
        public string start_date_time_text { get; set; }
        public string end_date_time { get; set; }
        public string end_date_time_text { get; set; }
        public decimal? total_hour { get; set; }
        public string total_hour_desc { get; set; }
        public decimal? parking_amt { get; set; }
        public decimal? special_paid_amt { get; set; }
        public decimal? special_unpaid_amt { get; set; }
        public decimal? total_amt { get; set; }
        public decimal? pay_amt { get; set; }
        public string enter_car_type_cd { get; set; }
        public string enter_car_type_desc { get; set; }
        public string image_url { get; set; }
        public string enter_image_path_file { get; set; }        
        public string booth_image_path { get; set; }
        public string enter_image { get; set; }
        public string car_type_cd { get; set; }
        public string car_type_desc { get; set; }
        public decimal? policy_id { get; set; }
        public decimal? enter_booth_id { get; set; }
        public string enter_booth_desc { get; set; }
        public decimal? enter_amt { get; set; }
        public decimal? enter_notpay_amt { get; set; }
        public decimal? lost_card_amt { get; set; }
        public bool has_special { get; set; }
        public decimal? discount_amt { get; set; }        
        public decimal? discount_hour { get; set; }
        public string discount_flag { get; set; }
        public string sticker_flag { get; set; }
        public string slip_no { get; set; }        
        public string receipt_no { get; set; }
        public string creation_by { get; set;  }

        public string discount_time { get; set; }
        public string discount_time_text { get; set; }
        public string discount_id { get; set; }
        public string discount_reason_cd { get; set; }
        public string discount_reason_desc { get; set; }

        public List<CarInDiscountDto> discount { get; set; }

        public string process_flag { get; set; }
        
    }
}
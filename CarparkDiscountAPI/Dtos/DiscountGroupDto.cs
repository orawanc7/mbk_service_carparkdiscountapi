﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountGroupDto
    {
        public string discount_group_id { get; set; }
        public string discount_group_desc { get; set; }
    }
}
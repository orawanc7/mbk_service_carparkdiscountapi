﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.CompanyService
{
    public class Company
    {
        [XmlElement(ElementName = "Company_Code")]
        public string CompanyCode { get; set; }

        [XmlElement(ElementName = "Company_ParentCode")]
        public string CompanyParentCode { get; set; }

        [XmlElement(ElementName = "Company_NameTH")]
        public string CompanyNameTh { get; set; }

        [XmlElement(ElementName = "Company_ShortNameTH")]
        public string CompanyShortNameTh { get; set; }

        [XmlElement(ElementName = "Company_NameEN")]
        public string CompanyNameEn { get; set; }

        [XmlElement(ElementName = "Company_ShortNameEN")]
        public string CompanyShortNameEn { get; set; }
          
    }
}                
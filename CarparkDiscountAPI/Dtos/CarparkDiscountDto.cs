﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class CarparkDiscountDto
    {
        public string SeqId { get; set; }
        public string DiscountId { get; set; }  
        public string ReasonCd { get; set; }
        public string UserId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountAppCodeRequestDto
    {
        public string LocationCode { get; set; }
        public long SeqId { get; set; } /* car_park_master.seq_id */
        public string Code { get; set; } /* buzzebee code */
        public string UserId { get; set; }

        public string PublicFlag { get; set; }
      
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountDto
    {
        public string discount_id { get; set; }
        public string discount_desc { get; set; }
        public Decimal? receipt_start_amt { get; set; }
        public Decimal? receipt_end_amt { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountAppErrorDto
    {
        public DiscountAppErrorDetailDto error { get; set; }
    }
}
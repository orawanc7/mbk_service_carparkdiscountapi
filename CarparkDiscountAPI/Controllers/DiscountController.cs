﻿using CarparkDiscountAPI.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace CarparkDiscountAPI.Controllers
{
    public class DiscountController : BaseWithTokenController
    {
        public string FileName { get; set; }

        [HttpPost]
        [Route("api/DiscountApp")]
        public IHttpActionResult ProcessDiscount(DiscountAppCodeRequestDto discountAppCodeRequestDto)
        {
            var result = new ResultDto();

            FileName = "Discount" + DateTime.Now.ToString("yyyyMMdd") + ".txt";            

            try
            {
                Extensions.DiscountLogExtension.Write("Start Discount " +
                "Location " + discountAppCodeRequestDto.LocationCode + " " +
                "Seq " + discountAppCodeRequestDto.SeqId + " " +
                "Code " + discountAppCodeRequestDto.Code + " " + 
                "PublicFlag " + discountAppCodeRequestDto.PublicFlag 
                , FileName);

                string apiUrl = this.getApi(discountAppCodeRequestDto.LocationCode);

                DiscountAppCodeDto discountAppCodeDto = this.getDiscountAppCode(apiUrl, discountAppCodeRequestDto.Code);

                if (discountAppCodeDto == null)
                {
                    Extensions.DiscountLogExtension.Write("Not found " + discountAppCodeRequestDto.Code, FileName);

                    result.Status = false;
                    result.StatusCode = "404";
                    result.Message = "ไม่พบรหัสส่วนลดนี้";

                }
                else
                {
                    if (discountAppCodeDto.car_seq_id != null)
                    {
                        Extensions.DiscountLogExtension.Write("Duplicate code " + discountAppCodeRequestDto.Code, FileName);

                        result.Status = false;
                        result.StatusCode = "409";
                        result.Message = "รหัสได้ถูกใช้ไปแล้ว";
                    }
                    else
                    {
                        ResultServiceDto checkCanusDiscountDto = this.checkCanUseDiscountAppCode(apiUrl, discountAppCodeRequestDto.SeqId.ToString(), discountAppCodeDto.discount_id);

                        if (checkCanusDiscountDto.status)
                        {
                            var responseCall = this.CallDiscountApp(discountAppCodeDto);
                            if (responseCall != null)
                            {
                                if (responseCall.Status)
                                {
                                    var data = (DiscountAppResultDto)responseCall.Data;
                                    var responseSave = this.SaveDiscount(apiUrl,
                                        discountAppCodeRequestDto.SeqId,
                                        discountAppCodeDto.discount_id,
                                        discountAppCodeDto.app_code,
                                        discountAppCodeRequestDto.PublicFlag,
                                        data,
                                        discountAppCodeRequestDto.UserId);
                                    result = responseSave;
                                }
                                else
                                {
                                    result.Status = responseCall.Status;
                                    result.StatusCode = responseCall.StatusCode;
                                    result.Message = responseCall.Message;
                                }
                            }
                        } else
                        {
                            Extensions.DiscountLogExtension.Write("Can't Use discount " + discountAppCodeRequestDto.Code,checkCanusDiscountDto.error_msg);

                            result.Status = false;
                            result.StatusCode = "409";
                            result.Message = checkCanusDiscountDto.error_msg;
                        }                        
                    }
                }
            } catch (Exception e)
            {                
                result.Message = e.Message;
            }
            return Json(result);
        }

        private string getApi(string appCode)
        {
            List<string> url = null;
            string apiUrl = String.Empty;
            if (ConfigurationManager.AppSettings["ApiURL"] != null)
            {
                url = ConfigurationManager.AppSettings["ApiURL"].Split(',').ToList();
                foreach (var u in url)
                {
                    var arr = u.Split('|');
                    if (arr[0].Equals(appCode))
                    {
                        return arr[1];
                    }
                }
            }

            return String.Empty;
        }
        
        private DiscountAppCodeDto getDiscountAppCode(string urlApi,string code)
        {
            string serviceUrl = "DiscountAppCode?code={p_code}";

            serviceUrl = serviceUrl.Replace("{p_code}", code);

            var url = GetUrl(urlApi, serviceUrl);            

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "GET";

            var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpResponseItem.GetResponseStream();

            DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(DiscountAppCodeDto));
            DiscountAppCodeDto objResponse = (DiscountAppCodeDto)dataContractJsonSerializer.ReadObject(stream);

            return objResponse;
        }

        private ResultServiceDto checkCanUseDiscountAppCode(string urlApi, string seqId, string discountId)
        {
            string serviceUrl = "CarParkDiscount/CanUseDiscount?p_seq_id={p_seq_id}&p_discount_id={p_discount_id}";

            serviceUrl = serviceUrl.Replace("{p_seq_id}", seqId);
            serviceUrl = serviceUrl.Replace("{p_discount_id}", discountId);

            var url = GetUrl(urlApi, serviceUrl);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "GET";

            var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
            Stream stream = httpResponseItem.GetResponseStream();

            DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(ResultServiceDto));
            ResultServiceDto objResponse = (ResultServiceDto)dataContractJsonSerializer.ReadObject(stream);

            return objResponse;
        }

        private ResultDto SaveDiscount(string urlApi, 
            decimal seqId,
            string discountId,
            decimal appCode,
            string publicFlag,
            DiscountAppResultDto codeResult,
            string userId)
        {
            var result = new ResultDto();

            string serviceUrl = "DiscountAppCode";

            //string serviceUrl = "DiscountAppCode?seq_id={seq_id}" +
            //  "&discount_id={discount_id}" +
            //  "&app_code={app_code}" +
            //  "&campaign_id={campaign_id}" +
            //  "&campaign_name={campaign_name}" +
            //  "&expire_in={expire_in}" +
            //  "&privilege_message={privilege_message}" +
            //  "&redeem_count={redeem_count}" +
            //  "&item_number={item_number}" +
            //  "&serial={serial}" +
            //  "&app_id={app_id}" +
            //  "&use_date={use_date}" +
            //  "&use_count={use_count}" +
            //  "&user_id={user_id}" +
            //  "&first_name={first_name}" +
            //  "&last_name={last_name}" +
            //  "&id_card={id_card}" +
            //  "&branch_id={branch_id}" +
            //  "&brand_id={brand_id}" +
            //  "&terminal_id={terminal_id}" +
            //  "&merchant_id={merchant_id}" +
            //  "&point_per_unit={point_per_unit}" +
            //  "&price_per_unit={price_per_unit}" +
            //  "&original_price={original_price}" +
            //  "&total_points={total_points}" +
            //  "&discount_price={discount_price}" +
            //  "&agency_id={agency_id}" +
            //  "&location_agency_id={location_agency_id}" +
            //  "&category_id={category_id}" +
            //  "&partition_key={partition_key}" +
            //  "&row_key={row_key}" +
            //  "&user={user}";

            //serviceUrl = serviceUrl.Replace("{seq_id}", String.Format("{0:0}",seqId));
            //serviceUrl = serviceUrl.Replace("{discount_id}", discountId);
            //serviceUrl = serviceUrl.Replace("{app_code}", String.Format("{0:0}", appCode));
            //serviceUrl = serviceUrl.Replace("{campaign_id}", String.Format("{0:0}", codeResult.campaignId));
            //serviceUrl = serviceUrl.Replace("{campaign_name}", codeResult.campaignName);
            //serviceUrl = serviceUrl.Replace("{expire_in}", String.Format("{0:0}",codeResult.expireIn));
            //serviceUrl = serviceUrl.Replace("{privilege_message}", ((codeResult.privilegeMessage==null)?"": codeResult.privilegeMessage));
            //serviceUrl = serviceUrl.Replace("{redeem_count}", String.Format("{0:0}", codeResult.redeemCount));
            //serviceUrl = serviceUrl.Replace("{item_number}", String.Format("{0:0}", codeResult.itemnumber));
            //serviceUrl = serviceUrl.Replace("{serial}", String.Format("{0:0}", codeResult.serial));
            //serviceUrl = serviceUrl.Replace("{app_id}", codeResult.appId);
            //serviceUrl = serviceUrl.Replace("{use_date}", codeResult.useDate);
            //serviceUrl = serviceUrl.Replace("{use_count}", String.Format("{0:0}", codeResult.useCount));
            //serviceUrl = serviceUrl.Replace("{user_id}", codeResult.userId);
            //serviceUrl = serviceUrl.Replace("{first_name}", codeResult.firstName);
            //serviceUrl = serviceUrl.Replace("{last_name}", codeResult.lastName);
            //serviceUrl = serviceUrl.Replace("{id_card}", ((codeResult.idcard==null)?"": codeResult.idcard));
            //serviceUrl = serviceUrl.Replace("{branch_id}", codeResult.branchId);
            //serviceUrl = serviceUrl.Replace("{brand_id}", codeResult.brandId);
            //serviceUrl = serviceUrl.Replace("{terminal_id}", codeResult.terminalId);
            //serviceUrl = serviceUrl.Replace("{merchant_id}", codeResult.merchant);
            //serviceUrl = serviceUrl.Replace("{point_per_unit}", codeResult.pointPerUnit.ToString());
            //serviceUrl = serviceUrl.Replace("{price_per_unit}", codeResult.pricePerUnit.ToString());
            //serviceUrl = serviceUrl.Replace("{original_price}", ((codeResult.originalPrice==null)?"": codeResult.originalPrice.ToString()));
            //serviceUrl = serviceUrl.Replace("{total_points}", codeResult.totalPoints.ToString());
            //serviceUrl = serviceUrl.Replace("{discount_price}", codeResult.discountPrice.ToString());
            //serviceUrl = serviceUrl.Replace("{agency_id}", String.Format("{0:0}", codeResult.agencyId));
            //serviceUrl = serviceUrl.Replace("{location_agency_id}", String.Format("{0:0}", codeResult.locationAgencyId));
            //serviceUrl = serviceUrl.Replace("{category_id}", String.Format("{0:0}", codeResult.categoryId));
            //serviceUrl = serviceUrl.Replace("{partition_key}", codeResult.partitionKey);
            //serviceUrl = serviceUrl.Replace("{row_key}", codeResult.rowKey);
            //serviceUrl = serviceUrl.Replace("{user}", userId);

            var discountAppSaveDto = new DiscountAppSaveDto();
            discountAppSaveDto.seq_id = seqId;
            discountAppSaveDto.discount_id = discountId;
            discountAppSaveDto.app_code = appCode;
            discountAppSaveDto.public_flag = publicFlag;
            discountAppSaveDto.campaign_id = (decimal) codeResult.campaignId;
            discountAppSaveDto.campaign_name = codeResult.campaignName;
            if (codeResult.expireIn!=null)
            {
                discountAppSaveDto.expire_in = (decimal)codeResult.expireIn;
            }
            
            discountAppSaveDto.privilege_message = codeResult.privilegeMessage;
            discountAppSaveDto.redeem_count = (decimal ) codeResult.redeemCount;
            discountAppSaveDto.item_number= (decimal) codeResult.itemnumber;
            discountAppSaveDto.serial= Convert.ToDecimal(codeResult.serial);
            discountAppSaveDto.app_id = codeResult.appId;
            discountAppSaveDto.use_date =  codeResult.useDate;
            discountAppSaveDto.use_count= (decimal) codeResult.useCount;
            discountAppSaveDto.user_id = codeResult.userId;
            discountAppSaveDto.first_name = codeResult.firstName;
            discountAppSaveDto.last_name =  codeResult.lastName;
            discountAppSaveDto.id_card= codeResult.idcard;
            discountAppSaveDto.branch_id = codeResult.branchId;
            discountAppSaveDto.brand_id =  codeResult.brandId;
            discountAppSaveDto.terminal_id =  codeResult.terminalId;
            discountAppSaveDto.merchant_id = codeResult.merchant;
            discountAppSaveDto.point_per_unit = codeResult.pointPerUnit;
            discountAppSaveDto.price_per_unit =  codeResult.pricePerUnit;
            if (codeResult.originalPrice!=null)
            {
                discountAppSaveDto.original_price = codeResult.originalPrice;
            }            
            discountAppSaveDto.total_points = codeResult.totalPoints;
            discountAppSaveDto.discount_price = codeResult.discountPrice;
            discountAppSaveDto.agency_id =  codeResult.agencyId;
            discountAppSaveDto.location_agency_id = codeResult.locationAgencyId;
            discountAppSaveDto.category_id = codeResult.categoryId;
            discountAppSaveDto.partition_key =  codeResult.partitionKey;
            discountAppSaveDto.row_key = codeResult.rowKey;
            discountAppSaveDto.user = userId;

            var url = GetUrl(urlApi, serviceUrl);

            Extensions.DiscountLogExtension.Write("SaveDiscount with url " +
                            url
                            , FileName);
            Extensions.DiscountLogExtension.Write("SaveDiscount with data " +
                            JsonConvert.SerializeObject(discountAppSaveDto)
                            , FileName);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Method = "POST";
            

            string json = JsonConvert.SerializeObject(discountAppSaveDto);
                      
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);

                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                Stream stream = httpResponse.GetResponseStream();

                DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(ResultServiceDto));
                ResultServiceDto objResponse = (ResultServiceDto)dataContractJsonSerializer.ReadObject(stream);


                Extensions.DiscountLogExtension.Write("Result from SaveDiscount " +
                            JsonConvert.SerializeObject(objResponse)
                            , FileName);
                if (objResponse != null)
                {

                    result.Status = objResponse.status;
                    result.Data = objResponse.error_msg;
                }
                else
                {
                    result.Status = true;
                    result.Message = String.Empty;
                    result.Data = null;
                }
            }
              
            return result;
        }

        private ResultDto CallDiscountApp (DiscountAppCodeDto discountAppCodeDto)
        {
                        
            string serviceUrl = String.Empty;
            var result = new ResultDto();

            string discountAppUrl = String.Empty;


            if (ConfigurationManager.AppSettings["DiscountAppURL"] != null)
            {
                discountAppUrl = ConfigurationManager.AppSettings["DiscountAppURL"];
            }

            serviceUrl = discountAppUrl + "?token={p_token}&code={p_code}&terminalId={p_terminal_id}&branchId={p_branch_id}&brandId={p_brand_id}&amount=0";

            serviceUrl = serviceUrl.Replace("{p_token}", discountAppCodeDto.discount_app_token);
            serviceUrl = serviceUrl.Replace("{p_code}", string.Format("{0:0}", discountAppCodeDto.app_code));
            serviceUrl = serviceUrl.Replace("{p_terminal_id}", discountAppCodeDto.discount_app_terminal_id);
            serviceUrl = serviceUrl.Replace("{p_branch_id}", discountAppCodeDto.discount_app_branch_id);
            serviceUrl = serviceUrl.Replace("{p_brand_id}", discountAppCodeDto.discount_app_brand_id);

            Extensions.DiscountLogExtension.Write("Call Service with url " + serviceUrl, FileName);

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(serviceUrl);            
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json";

            try { 
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Flush();
                    streamWriter.Close();

                    WebResponse httpResponse = httpWebRequest.GetResponse();
                    //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponse.GetResponseStream();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = int.MaxValue;
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                    string strJson = readStream.ReadToEnd();

                    Extensions.DiscountLogExtension.Write("Result from service (Before convert to object) " +
                        strJson
                        , FileName);

                    DiscountAppResultDto objResponse = (DiscountAppResultDto)js.Deserialize(strJson, typeof(DiscountAppResultDto));

                    if (objResponse.expireIn < 0)
                    {
                        result.Status = false;
                        result.StatusCode = "E1001";
                        result.Message = "Code นี้หมดอายุ ไม่สามารถใช้งานได้";
                    }
                    else
                    {

                        //DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(DiscountAppResultDto));
                        //DiscountAppResultDto objResponse = (DiscountAppResultDto)dataContractJsonSerializer.ReadObject(stream);

                        Extensions.DiscountLogExtension.Write("Result from service (After convert) " +
                                JsonConvert.SerializeObject(objResponse)
                                , FileName);

                        result.Status = true;
                        result.Data = objResponse;
                    }
                }                       
            } catch (WebException webEx)
            {
                Extensions.DiscountLogExtension.Write("Result from service (error WebException webEx) " +
                            webEx.Message,
                            FileName);


                try
                {
                    if (webEx.Response != null)
                    {
                        using (WebResponse response = webEx.Response)
                        {
                            var httpResponse = (HttpWebResponse)response;
                            Stream stream = httpResponse.GetResponseStream();

                            JavaScriptSerializer js = new JavaScriptSerializer();
                            js.MaxJsonLength = int.MaxValue;
                            StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                            string strJson = readStream.ReadToEnd();

                            Extensions.DiscountLogExtension.Write("Result from service (error) (Before convert to object) " +
                                strJson
                                , FileName);

                            //{"error":{"id":412,"message":"PreconditionFailed","code":412,"type":"buzzebees"}}
                            DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(DiscountAppErrorDto));
                            DiscountAppErrorDto objResponse = (DiscountAppErrorDto)dataContractJsonSerializer.ReadObject(stream);

                            Extensions.DiscountLogExtension.Write("Result from service (error) " +
                            JsonConvert.SerializeObject(objResponse)
                            , FileName);

                            result.Status = false;
                            result.StatusCode = objResponse.error.code.ToString();

                            if (objResponse.error.id== 2048)
                            {
                                result.Message = "Code นี้มีการใช้ไปแล้ว";
                            } else
                            {
                                result.Message = objResponse.error.message;
                            }
                            
                            /*
                            HttpWebResponse httpResponse = (HttpWebResponse)response;
                            using (Stream responseData = response.GetResponseStream())
                            using (var reader = new StreamReader(responseData))
                            {
                                string text = reader.ReadToEnd();                        
                                Console.WriteLine(text);
                            }
                            */
                        }
                    } else
                    {
                        result.Status = false;
                        result.Message = webEx.Message;
                    }
                } catch (Exception ex)
                {

                    Extensions.DiscountLogExtension.Write("Result from service (error Exception ex) " +
                            ex.Message,
                            FileName);

                    result.Status = false;
                    if (ex.InnerException !=null)
                    {
                        result.Message = ex.Message + "inner " + ex.InnerException.Message;
                    } else
                    {
                        result.Message = ex.Message ;
                    }
                    
                }
            }            

            return result;
        }
    }        
}

﻿using CarparkDiscountAPI.Dtos.LoginService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.LoginService
{
    [XmlRoot(ElementName = "dsService")]
    public class DsService
    {
        [XmlElement(ElementName = "Error")]
        public Error Error { get; set; }

        [XmlElement(ElementName = "LoginInfo")]
        public LoginInfo LoginInfo { get; set; }
    }    
}
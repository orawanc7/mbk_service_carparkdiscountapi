﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class ResultDto
    {
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public object Data2 { get; set; }
        public object Data3 { get; set; }
        public object Data4 { get; set; }
    }
}
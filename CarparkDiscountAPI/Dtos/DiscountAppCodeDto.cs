﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountAppCodeDto
    {
        public string discount_app_token { get; set; }
        public string discount_app_terminal_id { get; set; }
        public string discount_app_branch_id { get; set; }
        public string discount_app_brand_id { get; set; }
        public string discount_id { get; set; }
        public decimal app_code { get; set; }
        public decimal? car_seq_id { get; set; }
    }
}
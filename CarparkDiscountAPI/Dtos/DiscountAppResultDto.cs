﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountAppResultDto
    {
        public bool success { get; set; }
        public decimal? expireIn { get; set; }
        public bool isUsed { get; set; }
        public decimal? campaignId { get; set; }
        public decimal? itemnumber { get; set; }
        public string privilegeMessage { get; set; }
        public decimal? redeemCount { get; set; }
        public string serial { get; set; }
        public decimal? useCount { get; set; }
        public string useDate { get; set; }
        public string userId { get; set; }
        public string branchId { get; set; }
        public string brandId { get; set; }
        public string terminalId { get; set; }
        public string merchant { get; set; }
        public decimal? pointPerUnit { get; set; }
        public decimal? pricePerUnit { get; set; }
        public decimal? originalPrice { get; set; }
        public decimal? discountPrice { get; set; }
        public string campaignName { get; set; }
        public string appId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string idcard { get; set; }
        public decimal? totalPoints { get; set; }
        public decimal? agencyId { get; set; }
        public decimal? locationAgencyId { get; set; }
        public decimal? categoryId { get; set; }
        public string partitionKey { get; set; }
        public string rowKey { get; set; }
    }
}
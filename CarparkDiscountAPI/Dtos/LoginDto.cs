﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class LoginDto
    {
        public string UserId { get; set; }
        public string UserPwd { get; set; }
        public string IpAddr { get; set; }
    }
}
﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CarparkDiscountAPI.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://tls-devcollsrv1.mbk-center.mbk:803/LoginService.asmx")]
        public string CarparkDiscountAPI_mbk_sc_LoginService_LoginService {
            get {
                return ((string)(this["CarparkDiscountAPI_mbk_sc_LoginService_LoginService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://tls-devcollsrv1.mbk-center.mbk:803/GetConnectionService.asmx")]
        public string CarparkDiscountAPI_mbk_sc_Connection_GetConnectionService {
            get {
                return ((string)(this["CarparkDiscountAPI_mbk_sc_Connection_GetConnectionService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://tls-devcollsrv1.mbk-center.mbk:803/LogoutService.asmx")]
        public string CarparkDiscountAPI_mbk_sc_LogoutService_LogoutService {
            get {
                return ((string)(this["CarparkDiscountAPI_mbk_sc_LogoutService_LogoutService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://tls-devcollsrv1.mbk-center.mbk:803/GetUserInformation.asmx")]
        public string CarparkDiscountAPI_mbk_sc_UserInfo_GetUserInformation {
            get {
                return ((string)(this["CarparkDiscountAPI_mbk_sc_UserInfo_GetUserInformation"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://tls-devcollsrv1.mbk-center.mbk:803/GetCompanyService.asmx")]
        public string CarparkDiscountAPI_mbk_sc_Company_GetCompanyService {
            get {
                return ((string)(this["CarparkDiscountAPI_mbk_sc_Company_GetCompanyService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://tls-devcollsrv1.mbk-center.mbk:803/GetMenuService.asmx")]
        public string CarparkDiscountAPI_mbk_sc_Menu_GetMenuService {
            get {
                return ((string)(this["CarparkDiscountAPI_mbk_sc_Menu_GetMenuService"]));
            }
        }
    }
}

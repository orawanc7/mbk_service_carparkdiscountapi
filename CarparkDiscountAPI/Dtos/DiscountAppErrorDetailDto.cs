﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountAppErrorDetailDto
    {
        public int id { get; set; }
        public string message { get; set; }
        public int code { get; set; }
        public string type { get; set; }
    }
}
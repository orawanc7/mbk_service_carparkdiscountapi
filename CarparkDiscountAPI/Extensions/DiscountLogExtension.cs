﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using System.Web.Hosting;

namespace CarparkDiscountAPI.Extensions
{
    public class DiscountLogExtension
    {
        public static void Write(string message,string fileName)
        {

            //string logFilePath = String.Format(@"{0}\log\" + fileName, System.IO.Path.GetDirectoryName(
            //System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase));

            string logFilePath = AppDomain.CurrentDomain.BaseDirectory + "log\\" + fileName;


            try
            {
                using (StreamWriter w = File.AppendText(logFilePath))
                {
                    w.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                        DateTime.Now.ToLongDateString());                    
                    w.WriteLine("  :{0}", message);                    
                }
            }
            catch (Exception ex)
            {                
                //throw ex;
            }
        }

        private void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
            }
        }
    }
}
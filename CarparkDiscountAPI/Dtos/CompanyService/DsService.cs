﻿using CarparkDiscountAPI.Dtos.LoginService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.CompanyService
{
    [XmlRoot(ElementName = "dsService")]
    public class DsService
    {
        [XmlElement(ElementName = "Error")]
        public Error Error { get; set; }

        [XmlElement(ElementName = "Company")]
        public List<Company> Companies { get; set; }
    }    
}
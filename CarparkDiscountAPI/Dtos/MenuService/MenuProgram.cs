﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.MenuService
{
    public class MenuProgram
    {
        [XmlElement(ElementName = "Program_Code")]
        public string ProgramCode { get; set; }

        [XmlElement(ElementName = "Program_SysCode")]
        public string ProgramSysCode { get; set; }

        [XmlElement(ElementName = "Program_NameTH")]
        public string SystemName { get; set; } /* ชื่อระบบ */

        [XmlElement(ElementName = "Program_NameEN")]
        public string CompanyCode { get; set; } /* รหัสบริษัท */

        [XmlElement(ElementName = "Program_ProTypeCode")]
        public string ProgramProTypeCode { get; set; }

        [XmlElement(ElementName = "Program_Level")]
        public string ProgramLevel { get; set; }

        [XmlElement(ElementName = "Program_ParentCode")]
        public string ProgramParentCode { get; set; }

        [XmlElement(ElementName = "Program_TargetUrl")]
        public string Url { get; set; } /* URL API */

        [XmlElement(ElementName = "Program_Order")]
        public string ProgramOrder { get; set; }        

    }
}                
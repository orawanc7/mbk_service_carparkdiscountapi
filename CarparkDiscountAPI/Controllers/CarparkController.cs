﻿using CarparkDiscountAPI.Dtos;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Web.Http;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using System.Text;
using CarparkDiscountAPI.Dtos.CompanyService;

namespace CarparkDiscountAPI.Controllers
{
    public class CarparkController : BaseController
    {
        [HttpGet]
        [Route("api/Carpark/ListDomain")]
        public IHttpActionResult ListDomain ()
        {
            var result = new ResultDto();

            if (ConfigurationManager.AppSettings["DomainList"]!=null)
            {
                var domainList = ConfigurationManager.AppSettings["DomainList"].Split(',');
                result.Status = true;
                result.Data = domainList;
            } else
            {
                result.Status = false;
            }
                        

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListCompany")]
        public IHttpActionResult ListCompany()
        {
            var result = new ResultDto();

            string strLoginId = String.Empty;                
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                var service = new mbk.sc.Company.GetCompanyService();
                var resultService = service.GetAllCompany(strLoginId, this.SysCode, strIpAddress);

                //var companies = new List<Dtos.CompanyService.Company>();
                //var company = new Dtos.CompanyService.Company();
                //company.CompanyCode = "001";
                //company.CompanyNameTh = "เอ็มบีเค จำกัด";
                //companies.Add(company);

                //company = new Dtos.CompanyService.Company();
                //company.CompanyCode = "002";
                //company.CompanyNameTh = "The nine";
                //companies.Add(company);

                //result.Status = true;
                //result.Data = companies;

                using (var resultReader = new StringReader(resultService))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Dtos.CompanyService.DsService));

                    Dtos.CompanyService.DsService resultConvert = (Dtos.CompanyService.DsService)serializer.Deserialize(resultReader);

                    if (resultConvert.Error.Code.Equals("1"))
                    {
                        /*result.Status = true;
                        result.Data = resultConvert.Companies;
                        */
                        string strUserId = GetUserId();
                        string strUserPassword = GetUserPwd();
                        result = GetCompanyMenu(resultConvert.Companies, strUserId, strUserPassword, strLoginId, strIpAddress);
                    }
                    else
                    {
                        result.Status = false;
                        result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
                    }
                }
                
            }            
            return Json(result);
        }


        private ResultDto GetCompanyMenu(List<Company> Companies, 
            
            string UserId,
            string UserPwd,
            string LoginId,
            string IpAddress)
        {

            var result = new ResultDto();

            foreach (var com in Companies)
            {
                var service = new mbk.sc.LoginService.LoginService();
                var resultService = service.LoginWithCompCode(this.SysCode, UserId, UserPwd, IpAddress, com.CompanyCode);

                var serviceMenu = new mbk.sc.Menu.GetMenuService();

                using (var resultReader = new StringReader(resultService))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Dtos.LoginService.DsService));

                    Dtos.LoginService.DsService resultConvert = (Dtos.LoginService.DsService)serializer.Deserialize(resultReader);

                    if (resultConvert.Error.Code.Equals("MES9993"))
                    {
                        /*                
                        <dsService>
                          <Error>
                            <Code>1</Code>
                            <MessageTH>SUCCESS</MessageTH>
                            <MessageEN>SUCCESS</MessageEN>
                          </Error>
                          <MenuProgram>
                            <Program_Code>HMCARPARK01</Program_Code>
                            <Program_SysCode>CARPARK1</Program_SysCode>
                            <Program_NameTH>hm_ลานรถยนต์_ส่วนลดบุคคล</Program_NameTH>
                            <Program_NameEN>hm_carpark_emp</Program_NameEN>
                            <Program_ProTypeCode>T</Program_ProTypeCode>
                            <Program_Level>1</Program_Level>
                            <Program_TargetUrl />
                            <Program_Order>1</Program_Order>
                          </MenuProgram>
                          <MenuProgram>
                            <Program_Code>MBKCARPARK03</Program_Code>
                            <Program_SysCode>CARPARK1</Program_SysCode>
                            <Program_NameTH>mbk_ลานรถยนต์_ส่วนลดapp</Program_NameTH>
                            <Program_NameEN>mbk_carpark_app</Program_NameEN>
                            <Program_ProTypeCode>T</Program_ProTypeCode>
                            <Program_Level>1</Program_Level>
                            <Program_TargetUrl />
                            <Program_Order>1</Program_Order>
                          </MenuProgram>
                        </dsService>
                         */


                        var resultServiceMenu = serviceMenu.GetMenu(LoginId, this.SysCode, IpAddress);

                        using (var resultReaderMenu = new StringReader(resultServiceMenu))
                        {
                            XmlSerializer serializerMenu = new XmlSerializer(typeof(Dtos.MenuService.DsService));

                            Dtos.MenuService.DsService resultConvertMenu = (Dtos.MenuService.DsService)serializerMenu.Deserialize(resultReaderMenu);

                            if (resultConvertMenu.Error.Code.Equals("1") && resultConvertMenu.Error.MessageEn.Equals("SUCCESS"))
                            {
                                List<Dtos.CompanyService.Company> companies = new List<Dtos.CompanyService.Company>();

                                /* เลือกเฉพาะ Company Code ที่สนใจ */
                                if (resultConvertMenu.MenuPrograms != null)
                                {
                                    foreach (var r in resultConvertMenu.MenuPrograms)
                                    {
                                        if (companies.Where(c => c.CompanyCode == r.Url).Count() == 0)
                                        {
                                            var section = ConfigurationManager.GetSection("company") as NameValueCollection;

                                            if (section[r.Url] != null)
                                            {
                                                var company = new Dtos.CompanyService.Company();
                                                company.CompanyCode = r.Url;
                                                company.CompanyNameTh = section[r.Url];
                                                companies.Add(company);
                                            }
                                        }
                                    }
                                }
                                result.Status = true;
                                result.Data = companies;
                            }
                            else
                            {
                                result.Status = false;
                                result.Message = (resultConvertMenu.Error.MessageTh == null) ? resultConvertMenu.Error.MessageEn : resultConvertMenu.Error.MessageTh;
                            }
                        }
                    }
                }

            }

            return result;
        }

        [HttpGet]
        [Route("api/Carpark/ListPermission/{CompCode}/{Emp_id}")]
        public IHttpActionResult ListPermission(string CompCode, string Emp_id)
        {
            var result = new ResultDto();
            var strEmp_id = Emp_id.Equals("0") ? "" : Emp_id;

            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();

                /*                
                <dsService>
                  <Error>
                    <Code>1</Code>
                    <MessageTH>SUCCESS</MessageTH>
                    <MessageEN>SUCCESS</MessageEN>
                  </Error>
                  <MenuProgram>
                    <Program_Code>HMCARPARK01</Program_Code>
                    <Program_SysCode>CARPARK1</Program_SysCode>
                    <Program_NameTH>hm_ลานรถยนต์_ส่วนลดบุคคล</Program_NameTH>
                    <Program_NameEN>hm_carpark_emp</Program_NameEN>
                    <Program_ProTypeCode>T</Program_ProTypeCode>
                    <Program_Level>1</Program_Level>
                    <Program_TargetUrl />
                    <Program_Order>1</Program_Order>
                  </MenuProgram>
                  <MenuProgram>
                    <Program_Code>MBKCARPARK03</Program_Code>
                    <Program_SysCode>CARPARK1</Program_SysCode>
                    <Program_NameTH>mbk_ลานรถยนต์_ส่วนลดapp</Program_NameTH>
                    <Program_NameEN>mbk_carpark_app</Program_NameEN>
                    <Program_ProTypeCode>T</Program_ProTypeCode>
                    <Program_Level>1</Program_Level>
                    <Program_TargetUrl />
                    <Program_Order>1</Program_Order>
                  </MenuProgram>
                </dsService>
                 */

                var service = new mbk.sc.Menu.GetMenuService();

                var resultService = service.GetMenu(strLoginId, this.SysCode, strIpAddress);

                using (var resultReader = new StringReader(resultService))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Dtos.MenuService.DsService));

                    Dtos.MenuService.DsService resultConvert = (Dtos.MenuService.DsService)serializer.Deserialize(resultReader);

                    if (resultConvert.Error.Code.Equals("1") && resultConvert.Error.MessageEn.Equals("SUCCESS"))
                    {
                        List<Dtos.MenuService.MenuProgram> resultEmp = new List<Dtos.MenuService.MenuProgram>();
                        List<Dtos.MenuService.MenuProgram> resultReceipt = new List<Dtos.MenuService.MenuProgram>();
                        List<Dtos.MenuService.MenuProgram> resultApp = new List<Dtos.MenuService.MenuProgram>();
                        List<Dtos.MenuService.MenuProgram> resultOther = new List<Dtos.MenuService.MenuProgram>();
                        List<Dtos.MenuService.MenuProgram> resultReceiptShop = new List<Dtos.MenuService.MenuProgram>();

                        var section = ConfigurationManager.GetSection("programList" + CompCode) as NameValueCollection;

                        /* เลือกเฉพาะ Company Code ที่เลือก */

                        List<Dtos.LocationPermissionDto> resultLocationPermission = new List<Dtos.LocationPermissionDto>();
                        if (resultConvert.MenuPrograms != null)
                        {
                            foreach (var r in resultConvert.MenuPrograms)
                            {
                                if (r.Url.CompareTo(CompCode)==0)
                                {

                                    if (section[r.ProgramCode]!=null)
                                    {
                                        var data = section[r.ProgramCode].Split(',');
                                        var companyCode = CompCode;
                                        var locationCode = data[0];
                                        var locationName = data[1];
                                        var permissionType = data[2];
                                        var url = data[3];                                        

                                        var locationPermission = resultLocationPermission.Where(c => c.LocationCode.Equals(locationCode)).FirstOrDefault();
                                        if (locationPermission == null)
                                        {

                                            locationPermission = new LocationPermissionDto();
                                            locationPermission.CompanyCode = companyCode;
                                            locationPermission.LocationCode = locationCode;
                                            locationPermission.LocationName = locationName;
                                            locationPermission.Url = url;
                                            locationPermission.HasEmp = false;
                                            locationPermission.HasReceipt = false;
                                            locationPermission.HasApp = false;
                                            locationPermission.HasOther = false;
                                            locationPermission.HasReceiptShop = false;

                                            if (permissionType.Equals("EMP")) { 
                                                if (!String.IsNullOrEmpty(strEmp_id))
                                                {
                                                    var resultGetDiscount = GetDiscount(url, strEmp_id);
                                                    if (resultGetDiscount.Status)
                                                    {
                                                        if (resultGetDiscount.Data!=null)
                                                        {
                                                            var discount = (DiscountDto)resultGetDiscount.Data;
                                                            if (discount != null)
                                                            {
                                                                locationPermission.HasEmp = true;
                                                                locationPermission.DiscountId = discount.discount_id;
                                                                locationPermission.DiscountDesc = discount.discount_desc;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            else if (permissionType.Equals("RECEIPT"))
                                            {
                                                locationPermission.HasReceipt = true;
                                            }
                                            else if (permissionType.Equals("APP"))
                                            {
                                                locationPermission.HasApp = true;
                                            }
                                            else if (permissionType.Equals("OTHER"))
                                            {
                                                locationPermission.HasOther = true;
                                            }
                                            else if (permissionType.Equals("RECEIPT_SHOP"))
                                            {
                                                locationPermission.HasReceiptShop = true;
                                            }
                                            resultLocationPermission.Add(locationPermission);
                                        } else
                                        {
                                            if (permissionType.Equals("EMP"))
                                            {
                                                locationPermission.HasEmp = true;
                                            }
                                            else if (permissionType.Equals("RECEIPT"))
                                            {
                                                locationPermission.HasReceipt = true;
                                            }
                                            else if (permissionType.Equals("APP"))
                                            {
                                                locationPermission.HasApp = true;
                                            }
                                            else if (permissionType.Equals("OTHER"))
                                            {
                                                locationPermission.HasOther = true;
                                            }
                                            else if (permissionType.Equals("RECEIPT_SHOP"))
                                            {
                                                locationPermission.HasReceiptShop = true;
                                            }
                                            
                                        }                                                                             
                                    }                                    
                                }
                            }
                        }

                        result.Status = true;
                        result.Data = resultLocationPermission;                        
                    }
                    else
                    {
                        result.Status = false;
                        result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
                    }
                }
            }
            /*
             * <dsService>
                  <Error>
                    <Code>1</Code>
                    <MessageTH>SUCCESS</MessageTH>
                    <MessageEN>SUCCESS</MessageEN>
                  </Error>
                  <DBConnection>
                    <CompCode>001</CompCode>
                    <SysCode>CARPARK</SysCode>
                    <DBMSType>Other</DBMSType>
                    <DBServer>vtltMr1XBvI=</DBServer>
                    <DBPort />
                    <DBName>vtltMr1XBvI=</DBName>
                    <UserName>vtltMr1XBvI=</UserName>
                    <Pwd>vtltMr1XBvI=</Pwd>
                    <ServiceName />
                    <ConnectionString>6pgNPblr4frXdz4TGcvDS2atowfTB+gEt2I0a3+lI+06pkPa4LcKCg==</ConnectionString>
                    <DBCode>MBK1</DBCode>
                  </DBConnection>
                  <DBConnection>
                    <CompCode>001</CompCode>
                    <SysCode>CARPARK</SysCode>
                    <DBMSType>Other</DBMSType>
                    <DBServer>vtltMr1XBvI=</DBServer>
                    <DBPort />
                    <DBName>vtltMr1XBvI=</DBName>
                    <UserName>vtltMr1XBvI=</UserName>
                    <Pwd>vtltMr1XBvI=</Pwd>
                    <ServiceName />
                    <ConnectionString>6pgNPblr4frXdz4TGcvDS2atowfTB+gEt2I0a3+lI+1oS35sd5m7sZH4qv00y5Kx</ConnectionString>
                    <DBCode>MBK2</DBCode>
                  </DBConnection>
                </dsService>
                */

            //var jsonResult = @"{
            //    ""Status"": true,
            //    ""Message"": null,
            //    ""Data"": [
            //        {
            //            ""CompCode"": ""001"",
            //            ""SysCode"": ""CARPARK"",
            //            ""DBMSType"": ""Other"",
            //            ""DBServer"": ""vtltMr1XBvI ="",
            //            ""DBPort"": """",
            //            ""DBName"": ""vtltMr1XBvI ="",
            //            ""UserName"": ""vtltMr1XBvI = "",
            //            ""Pwd"": ""vtltMr1XBvI ="",
            //            ""ServiceName"": """",
            //            ""ConnectionString"": ""http://192.168.168.4/carparkservice/api"",
            //            ""DBCode"": ""MBK1""
            //        },
            //        {
            //            ""CompCode"": ""001"",
            //            ""SysCode"": ""CARPARK"",
            //            ""DBMSType"": ""Other"",
            //            ""DBServer"": ""vtltMr1XBvI="",
            //            ""DBPort"": """",
            //            ""DBName"": ""vtltMr1XBvI="",
            //            ""UserName"": ""vtltMr1XBvI="",
            //            ""Pwd"": ""vtltMr1XBvI="",
            //            ""ServiceName"": """",
            //            ""ConnectionString"": ""http://192.168.168.4/carparkservice_motor/api"",
            //            ""DBCode"": ""MBK2""
            //        }
            //    ]
            //}";
            return Json(result);
        }
        

        //[HttpGet]
        //[Route("api/Carpark/ListLocation/{CompCode}/{Emp_id}")]
        //public IHttpActionResult ListLocation(string CompCode,string Emp_id)
        //{
        //    var result = new ResultDto();            

        //    string strLoginId = String.Empty;            
        //    string strIpAddress = string.Empty;
        //    result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
        //    if (result.Status)
        //    {
        //        result = new ResultDto();

        //        /*                
        //        <dsService>
        //          <Error>
        //            <Code>1</Code>
        //            <MessageTH>SUCCESS</MessageTH>
        //            <MessageEN>SUCCESS</MessageEN>
        //          </Error>
        //          <MenuProgram>
        //            <Program_Code>HMCARPARK01</Program_Code>
        //            <Program_SysCode>CARPARK1</Program_SysCode>
        //            <Program_NameTH>hm_ลานรถยนต์_ส่วนลดบุคคล</Program_NameTH>
        //            <Program_NameEN>hm_carpark_emp</Program_NameEN>
        //            <Program_ProTypeCode>T</Program_ProTypeCode>
        //            <Program_Level>1</Program_Level>
        //            <Program_TargetUrl />
        //            <Program_Order>1</Program_Order>
        //          </MenuProgram>
        //          <MenuProgram>
        //            <Program_Code>MBKCARPARK03</Program_Code>
        //            <Program_SysCode>CARPARK1</Program_SysCode>
        //            <Program_NameTH>mbk_ลานรถยนต์_ส่วนลดapp</Program_NameTH>
        //            <Program_NameEN>mbk_carpark_app</Program_NameEN>
        //            <Program_ProTypeCode>T</Program_ProTypeCode>
        //            <Program_Level>1</Program_Level>
        //            <Program_TargetUrl />
        //            <Program_Order>1</Program_Order>
        //          </MenuProgram>
        //        </dsService>
        //         */

        //        var service = new mbk.sc.Menu.GetMenuService();  
                
        //        var resultService = service.GetMenu(strLoginId, this.SysCode, strIpAddress);

        //        using (var resultReader = new StringReader(resultService))
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(Dtos.MenuService.DsService));

        //            Dtos.MenuService.DsService resultConvert = (Dtos.MenuService.DsService)serializer.Deserialize(resultReader);

        //            if (resultConvert.Error.Code.Equals("1") && resultConvert.Error.MessageEn.Equals("SUCCESS"))
        //            {
        //                List<Dtos.MenuService.MenuProgram> resultFilterDiscountId = new List<Dtos.MenuService.MenuProgram> ();
        //                List<Dtos.MenuService.MenuProgram> resultFilter = new List<Dtos.MenuService.MenuProgram>();

        //                /* เลือกเฉพาะ Company Code ที่สนใจ */
        //                if (resultConvert.MenuPrograms != null)
        //                {
        //                    foreach (var r in resultConvert.MenuPrograms)
        //                    {
        //                        if (r.CompanyCode.Equals(CompCode))
        //                        {
        //                            var resultGetDiscount = GetDiscount(r.Url, Emp_id);
        //                            if (resultGetDiscount.Status)
        //                            {
        //                                var discount = (DiscountDto) resultGetDiscount.Data;
        //                                if (discount != null)
        //                                {
        //                                    // Comment For Test
        //                                    r.DiscountId = discount.discount_id;
        //                                    r.DiscountDesc = discount.discount_desc;
        //                                    //r.DiscountId = "MBK003123";
        //                                    //r.DiscountDesc = "คุณเฉลิมพล  คงการุณ.";
        //                                    //r.Url = "http://192.168.168.4/carparkservice_motor/api";
        //                                    resultFilterDiscountId.Add(r);
        //                                }
        //                            }
        //                            resultFilter.Add(r);
        //                        }
        //                    }
        //                }

        //                result.Status = true;
        //                result.Data = resultFilterDiscountId;
        //                result.Data2 = resultFilter;
        //            }
        //            else
        //            {
        //                result.Status = false;
        //                result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
        //            }
        //        }                
        //    }
        //    /*
        //     * <dsService>
        //          <Error>
        //            <Code>1</Code>
        //            <MessageTH>SUCCESS</MessageTH>
        //            <MessageEN>SUCCESS</MessageEN>
        //          </Error>
        //          <DBConnection>
        //            <CompCode>001</CompCode>
        //            <SysCode>CARPARK</SysCode>
        //            <DBMSType>Other</DBMSType>
        //            <DBServer>vtltMr1XBvI=</DBServer>
        //            <DBPort />
        //            <DBName>vtltMr1XBvI=</DBName>
        //            <UserName>vtltMr1XBvI=</UserName>
        //            <Pwd>vtltMr1XBvI=</Pwd>
        //            <ServiceName />
        //            <ConnectionString>6pgNPblr4frXdz4TGcvDS2atowfTB+gEt2I0a3+lI+06pkPa4LcKCg==</ConnectionString>
        //            <DBCode>MBK1</DBCode>
        //          </DBConnection>
        //          <DBConnection>
        //            <CompCode>001</CompCode>
        //            <SysCode>CARPARK</SysCode>
        //            <DBMSType>Other</DBMSType>
        //            <DBServer>vtltMr1XBvI=</DBServer>
        //            <DBPort />
        //            <DBName>vtltMr1XBvI=</DBName>
        //            <UserName>vtltMr1XBvI=</UserName>
        //            <Pwd>vtltMr1XBvI=</Pwd>
        //            <ServiceName />
        //            <ConnectionString>6pgNPblr4frXdz4TGcvDS2atowfTB+gEt2I0a3+lI+1oS35sd5m7sZH4qv00y5Kx</ConnectionString>
        //            <DBCode>MBK2</DBCode>
        //          </DBConnection>
        //        </dsService>
        //        */

        //    //var jsonResult = @"{
        //    //    ""Status"": true,
        //    //    ""Message"": null,
        //    //    ""Data"": [
        //    //        {
        //    //            ""CompCode"": ""001"",
        //    //            ""SysCode"": ""CARPARK"",
        //    //            ""DBMSType"": ""Other"",
        //    //            ""DBServer"": ""vtltMr1XBvI ="",
        //    //            ""DBPort"": """",
        //    //            ""DBName"": ""vtltMr1XBvI ="",
        //    //            ""UserName"": ""vtltMr1XBvI = "",
        //    //            ""Pwd"": ""vtltMr1XBvI ="",
        //    //            ""ServiceName"": """",
        //    //            ""ConnectionString"": ""http://192.168.168.4/carparkservice/api"",
        //    //            ""DBCode"": ""MBK1""
        //    //        },
        //    //        {
        //    //            ""CompCode"": ""001"",
        //    //            ""SysCode"": ""CARPARK"",
        //    //            ""DBMSType"": ""Other"",
        //    //            ""DBServer"": ""vtltMr1XBvI="",
        //    //            ""DBPort"": """",
        //    //            ""DBName"": ""vtltMr1XBvI="",
        //    //            ""UserName"": ""vtltMr1XBvI="",
        //    //            ""Pwd"": ""vtltMr1XBvI="",
        //    //            ""ServiceName"": """",
        //    //            ""ConnectionString"": ""http://192.168.168.4/carparkservice_motor/api"",
        //    //            ""DBCode"": ""MBK2""
        //    //        }
        //    //    ]
        //    //}";
        //    return Json(result);
        //}


        //[HttpGet]
        //[Route("api/Carpark/ListLocation2/{CompCode}/{Emp_id}")]
        //public IHttpActionResult ListLocation2(string CompCode, String Emp_id)
        //{
        //    var result = new ResultDto();

        //    string strLoginId = String.Empty;
        //    string strIpAddress = string.Empty;
        //    result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
        //    if (result.Status)
        //    {
        //        result = new ResultDto();

        //        var service = new mbk.sc.Menu.GetMenuService();
        //        var resultService = service.GetMenu(strLoginId, this.SysCode, strIpAddress);

        //        using (var resultReader = new StringReader(resultService))
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(Dtos.MenuService.DsService));

        //            Dtos.MenuService.DsService resultConvert = (Dtos.MenuService.DsService)serializer.Deserialize(resultReader);

        //            if (resultConvert.Error.Code.Equals("1") && resultConvert.Error.MessageEn.Equals("SUCCESS"))
        //            {
        //                List<Dtos.MenuService.MenuProgram> resultFilter = new List<Dtos.MenuService.MenuProgram>();

                        
        //                result.Status = true;
        //                result.Data = resultConvert.MenuPrograms;
        //            }
        //            else
        //            {
        //                result.Status = false;
        //                result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
        //            }
        //        }
        //    }
            
        //    return Json(result);
        //}

        //[HttpGet]
        //[Route("api/Carpark/ListLocation/{CompCode}")]
        //public IHttpActionResult ListLocation(string CompCode)
        //{
        //    var result = new ResultDto();

        //    string strLoginId = String.Empty;
        //    string strIpAddress = string.Empty;
        //    result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
        //    if (result.Status)
        //    {
        //        result = new ResultDto();
        //        string compCode = GetCompCode();
        //        string ipAddress = GetIpAddress();
        //        if (!(compCode.Equals(String.Empty) && ipAddress.Equals(String.Empty)))
        //        {
        //            var service = new mbk.sc.Connection.GetConnectionService();
        //            var resultService = service.GetConnection(strLoginId, this.SysCode, CompCode, strIpAddress);

        //            using (var resultReader = new StringReader(resultService))
        //            {
        //                XmlSerializer serializer = new XmlSerializer(typeof(Dtos.ConnnectionService.DsService));

        //                Dtos.ConnnectionService.DsService resultConvert = (Dtos.ConnnectionService.DsService)serializer.Deserialize(resultReader);

        //                if (resultConvert.Error.Code.Equals("1"))
        //                {
        //                    result.Status = true;
        //                    result.Data = resultConvert.DBConnections;
        //                }
        //                else
        //                {
        //                    result.Status = false;
        //                    result.Message = (resultConvert.Error.MessageTh == null) ? resultConvert.Error.MessageEn : resultConvert.Error.MessageTh;
        //                }
        //            }
        //        }
        //    }
        //    /*
        //     * <dsService>
        //          <Error>
        //            <Code>1</Code>
        //            <MessageTH>SUCCESS</MessageTH>
        //            <MessageEN>SUCCESS</MessageEN>
        //          </Error>
        //          <DBConnection>
        //            <CompCode>001</CompCode>
        //            <SysCode>CARPARK</SysCode>
        //            <DBMSType>Other</DBMSType>
        //            <DBServer>vtltMr1XBvI=</DBServer>
        //            <DBPort />
        //            <DBName>vtltMr1XBvI=</DBName>
        //            <UserName>vtltMr1XBvI=</UserName>
        //            <Pwd>vtltMr1XBvI=</Pwd>
        //            <ServiceName />
        //            <ConnectionString>6pgNPblr4frXdz4TGcvDS2atowfTB+gEt2I0a3+lI+06pkPa4LcKCg==</ConnectionString>
        //            <DBCode>MBK1</DBCode>
        //          </DBConnection>
        //          <DBConnection>
        //            <CompCode>001</CompCode>
        //            <SysCode>CARPARK</SysCode>
        //            <DBMSType>Other</DBMSType>
        //            <DBServer>vtltMr1XBvI=</DBServer>
        //            <DBPort />
        //            <DBName>vtltMr1XBvI=</DBName>
        //            <UserName>vtltMr1XBvI=</UserName>
        //            <Pwd>vtltMr1XBvI=</Pwd>
        //            <ServiceName />
        //            <ConnectionString>6pgNPblr4frXdz4TGcvDS2atowfTB+gEt2I0a3+lI+1oS35sd5m7sZH4qv00y5Kx</ConnectionString>
        //            <DBCode>MBK2</DBCode>
        //          </DBConnection>
        //        </dsService>
        //        */

        //    //var jsonResult = @"{
        //    //    ""Status"": true,
        //    //    ""Message"": null,
        //    //    ""Data"": [
        //    //        {
        //    //            ""CompCode"": ""001"",
        //    //            ""SysCode"": ""CARPARK"",
        //    //            ""DBMSType"": ""Other"",
        //    //            ""DBServer"": ""vtltMr1XBvI ="",
        //    //            ""DBPort"": """",
        //    //            ""DBName"": ""vtltMr1XBvI ="",
        //    //            ""UserName"": ""vtltMr1XBvI = "",
        //    //            ""Pwd"": ""vtltMr1XBvI ="",
        //    //            ""ServiceName"": """",
        //    //            ""ConnectionString"": ""http://192.168.168.4/carparkservice/api"",
        //    //            ""DBCode"": ""MBK1""
        //    //        },
        //    //        {
        //    //            ""CompCode"": ""001"",
        //    //            ""SysCode"": ""CARPARK"",
        //    //            ""DBMSType"": ""Other"",
        //    //            ""DBServer"": ""vtltMr1XBvI="",
        //    //            ""DBPort"": """",
        //    //            ""DBName"": ""vtltMr1XBvI="",
        //    //            ""UserName"": ""vtltMr1XBvI="",
        //    //            ""Pwd"": ""vtltMr1XBvI="",
        //    //            ""ServiceName"": """",
        //    //            ""ConnectionString"": ""http://192.168.168.4/carparkservice_motor/api"",
        //    //            ""DBCode"": ""MBK2""
        //    //        }
        //    //    ]
        //    //}";
        //    return Json(result);
        //}

        [HttpGet]
        [Route("api/Carpark/ListCar")]        
        public IHttpActionResult ListCar ()
        {
            NameValueCollection qscoll = Request.RequestUri.ParseQueryString();
            string Car_no = String.Empty;
            string Prox_id = String.Empty;

            if (qscoll["car_no"] != null)
            {
                Car_no = qscoll["car_no"];
            }

            if (qscoll["prox_id"]!=null)
            {
                Prox_id = qscoll["prox_id"];
            }

            
            string serviceUrl = "CarOut?p_car_sno={p_car_sno}&p_prox_id={p_prox_id}";

            serviceUrl = serviceUrl.Replace("{p_car_sno}", Car_no);
            serviceUrl = serviceUrl.Replace("{p_prox_id}", Prox_id);

            var result = new ResultDto();

            string strLoginId = String.Empty;            
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId,  ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = int.MaxValue;
                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);

                    string strJson = readStream.ReadToEnd();

                    List<CarDto> objResponse = (List<CarDto>)js.Deserialize(strJson, typeof(List<CarDto>));

                    //var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    //Stream stream = httpResponseItem.GetResponseStream();

                    //DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<CarDto>));
                    //List<CarDto> objResponse = (List<CarDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        if (objResponse.Count() > 0)
                        {
                            foreach (var r in objResponse)
                            {
                                if (r.enter_image_path_file != null && r.image_url != null) { 
                                    if (r.enter_image_path_file.Length > 0 && r.image_url.Length > 0)
                                    {
                                        r.enter_image = DownloadImage(r.image_url + r.enter_image_path_file);
                                    }
                                }
                            }                            
                        }

                        result.Status = true;
                        result.Data = objResponse;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/GetCar/{Seq_id}/{Discount_id}")]
        public IHttpActionResult GetCar(string Seq_id, string Discount_id)
        {
            NameValueCollection qscoll = Request.RequestUri.ParseQueryString();
            string Car_no = String.Empty;
            string Car_type_cd = String.Empty;

            if (qscoll["car_no"] != null)
            {
                Car_no = qscoll["car_no"];
            }

            if (qscoll["car_type_cd"] != null)
            {
                Car_type_cd = qscoll["car_type_cd"];
            }

            string serviceUrl = "CarOut?p_seq_id={p_seq_id}&p_exit_date={p_exit_date}&p_discount_list={p_discount_list}&p_car_lno={p_car_lno}&p_car_type_cd={p_car_type_cd}";

            serviceUrl = serviceUrl.Replace("{p_seq_id}", Seq_id);
            serviceUrl = serviceUrl.Replace("{p_exit_date}", DateTime.Now.ToString());
            serviceUrl = serviceUrl.Replace("{p_car_lno}", Car_no);
            serviceUrl = serviceUrl.Replace("{car_type_cd}", Car_type_cd);
            serviceUrl = serviceUrl.Replace("{p_discount_list}", Discount_id);

            var result = new ResultDto();

            string strLoginId = String.Empty;            
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(CarDto));
                    CarDto objResponse = (CarDto)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                    else
                    {
                        result.Status = true;
                        result.Message = String.Empty;
                        result.Data = null;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/GetCarWithDiscount/{Seq_id}")]
        public IHttpActionResult GetCarWithDiscount(string Seq_id)
        {
            NameValueCollection qscoll = Request.RequestUri.ParseQueryString();
            string Car_no = String.Empty;
            string Car_type_cd = String.Empty;
            string Discount_id = String.Empty;

            if (qscoll["car_no"] != null)
            {
                Car_no = qscoll["car_no"];
            }

            if (qscoll["car_type_cd"] != null)
            {
                Car_type_cd = qscoll["car_type_cd"];
            }

            if (qscoll["discount_id"] != null)
            {
                Discount_id = qscoll["discount_id"];
            }

            string serviceUrl = "CarOut?p_seq_id={p_seq_id}&p_exit_date={p_exit_date}&p_discount_list={p_discount_list}&p_car_lno={p_car_lno}&p_car_type_cd={p_car_type_cd}";

            serviceUrl = serviceUrl.Replace("{p_seq_id}", Seq_id);
            serviceUrl = serviceUrl.Replace("{p_exit_date}", DateTime.Now.ToString());
            serviceUrl = serviceUrl.Replace("{p_car_lno}", Car_no);
            serviceUrl = serviceUrl.Replace("{p_car_type_cd}", Car_type_cd);
            serviceUrl = serviceUrl.Replace("{p_discount_list}", Discount_id);

            var result = new ResultDto();

            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(CarDto));
                    CarDto objResponse = (CarDto)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                    else
                    {
                        result.Status = true;
                        result.Message = String.Empty;
                        result.Data = null;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/GetCarNoDiscount/{Seq_id}")]
        public IHttpActionResult GetCarNoDiscount(string Seq_id)
        {
            NameValueCollection qscoll = Request.RequestUri.ParseQueryString();
            string Car_no = String.Empty;
            string Car_type_cd = String.Empty;

            if (qscoll["car_no"] != null)
            {
                Car_no = qscoll["car_no"];
            }

            if (qscoll["car_type_cd"] != null)
            {
                Car_type_cd = qscoll["car_type_cd"];
            }

            string serviceUrl = "CarOut?p_seq_id={p_seq_id}&p_exit_date={p_exit_date}&p_discount_list={p_discount_list}&p_car_lno={p_car_lno}&p_car_type_cd={p_car_type_cd}";

            serviceUrl = serviceUrl.Replace("{p_seq_id}", Seq_id);
            serviceUrl = serviceUrl.Replace("{p_exit_date}", DateTime.Now.ToString());            
            serviceUrl = serviceUrl.Replace("{p_car_lno}", Car_no);
            serviceUrl = serviceUrl.Replace("{p_car_type_cd}", Car_type_cd);
            serviceUrl = serviceUrl.Replace("{p_discount_list}", "");

            var result = new ResultDto();

            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(CarDto));
                    CarDto objResponse = (CarDto)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                    else
                    {
                        result.Status = true;
                        result.Message = String.Empty;
                        result.Data = null;
                    }
                }
            }

            return Json(result);
        }


        [HttpPost]
        [Route("api/Carpark/SaveDiscount")]
        public IHttpActionResult SaveDiscount(CarparkDiscountDto carparkDiscountDto)
        {

            string serviceUrl = "CarparkDiscount?p_seq_id={p_seq_id}&p_discount_id={p_discount_id}&p_reason_cd={p_reason_cd}&p_user={p_user}";            

            serviceUrl = serviceUrl.Replace("{p_seq_id}", carparkDiscountDto.SeqId);
            serviceUrl = serviceUrl.Replace("{p_discount_id}", carparkDiscountDto.DiscountId);
            serviceUrl = serviceUrl.Replace("{p_reason_cd}", carparkDiscountDto.ReasonCd);
            serviceUrl = serviceUrl.Replace("{p_user}", carparkDiscountDto.UserId);

            var result = new ResultDto();
            string strLoginId = String.Empty;            
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId,  ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {
                    //url = "http://localhost:1335/api/" + serviceUrl;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "POST";

                    
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {                        
                        streamWriter.Flush();
                        streamWriter.Close();

                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        Stream stream = httpResponse.GetResponseStream();

                        DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(ResultServiceDto));
                        ResultServiceDto objResponse = (ResultServiceDto)dataContractJsonSerializer.ReadObject(stream);
                        if (objResponse != null)
                        {
                            
                            result.Status = objResponse.status;
                            result.Message = objResponse.error_msg;
                        }
                        else
                        {
                            result.Status = true;
                            result.Message = String.Empty;
                            result.Data = null;
                        }
                    }                    
                }
            }

            return Json(result);
        }

        [HttpPost]
        [Route("api/Carpark/SaveDiscountReceipt")]
        public IHttpActionResult SaveDiscountReceipt(CarparkDiscountReceiptDto carparkDiscountReceiptDto)
        {
            string serviceUrl = "CarparkDiscountReceipt";


            var receipts = new List<dynamic>();

            foreach (var r in carparkDiscountReceiptDto.Receipts)
            {
                receipts.Add(
                    new
                    {
                        discount_group_id = r.ReceiptDiscountGroupId,
                        discount_group_name = r.ReceiptDiscountGroupName,
                        receipt_no = r.ReceiptReceiptNo,
                        receipt_amt = r.ReceiptReceiptAmt                        
                    }
                );
            }            
            var data = new
            {
                seq_id = carparkDiscountReceiptDto.SeqId,
                discount_id = carparkDiscountReceiptDto.DiscountId,
                old_discount_id = carparkDiscountReceiptDto.OldDiscountId,
                public_flag = carparkDiscountReceiptDto.PublicFlag,
                receipt_mode = carparkDiscountReceiptDto.ReceiptMode,
                receipts = receipts,
                user_id = carparkDiscountReceiptDto.UserId
            };

            JavaScriptSerializer js = new JavaScriptSerializer();
            string strJsonData = js.Serialize(data);

            var result = new ResultDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {
                    //url = "http://localhost:1335/api/" + serviceUrl;
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "POST";


                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(strJsonData);
                        streamWriter.Flush();
                        streamWriter.Close();

                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        Stream stream = httpResponse.GetResponseStream();

                        DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(ResultServiceDto));
                        ResultServiceDto objResponse = (ResultServiceDto)dataContractJsonSerializer.ReadObject(stream);
                        if (objResponse != null)
                        {

                            result.Status = objResponse.status;
                            result.Message = objResponse.error_msg;
                        }
                        else
                        {
                            result.Status = true;
                            result.Message = String.Empty;
                            result.Data = null;
                        }
                    }
                }
            }

            return Json(result);
        }

        [HttpPost]
        [Route("api/Carpark/DeleteDiscount")]
        public IHttpActionResult DeleteDiscount(CarparkDiscountDto carparkDiscountDto)
        {

            string serviceUrl = "CarparkDiscount?p_seq_id={p_seq_id}&p_discount_id={p_discount_id}&p_user={p_user}";

            serviceUrl = serviceUrl.Replace("{p_seq_id}", carparkDiscountDto.SeqId);
            serviceUrl = serviceUrl.Replace("{p_discount_id}", carparkDiscountDto.DiscountId);
            serviceUrl = serviceUrl.Replace("{p_user}", carparkDiscountDto.UserId);

            var result = new ResultDto();
            string strLoginId = String.Empty;            
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "DELETE";


                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Flush();
                        streamWriter.Close();

                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        Stream stream = httpResponse.GetResponseStream();

                        DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(ResultServiceDto));
                        ResultServiceDto objResponse = (ResultServiceDto)dataContractJsonSerializer.ReadObject(stream);
                        if (objResponse != null)
                        {

                            result.Status = objResponse.status;
                            result.Data = objResponse.error_msg;
                        }
                        else
                        {
                            result.Status = true;
                            result.Message = String.Empty;
                            result.Data = null;
                        }
                    }
                }
            }

            return Json(result);
        }


        [HttpGet]
        [Route("api/Carpark/ListDiscount/{Discount_id}")]
        public IHttpActionResult ListDiscount(string Discount_id)
        {
            string serviceUrl = "CarparkDiscount/ListDiscount/{Discount_id}";

            var values = ControllerContext.Request.GetQueryNameValuePairs();

            var keyPageNo = values.LastOrDefault(x => x.Key == "pageno").Value;
            var keyPageLength = values.LastOrDefault(x => x.Key == "pagelength").Value;

            decimal pageNo = 1;
            decimal pageLength = 10;
            decimal totalPage = 1;
            decimal totalRecord = 0;

            if (keyPageNo!=null)
            {
                pageNo = Convert.ToInt32(keyPageNo);
            }

            if (keyPageLength != null)
            {
                pageLength = Convert.ToInt32(keyPageLength);
            }


            serviceUrl = serviceUrl.Replace("{Discount_id}", Discount_id);

            var result = new ResultDto();
            var paging = new PagingDto();
            string strLoginId = String.Empty;            
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId,  ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = int.MaxValue;
                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);

                    string strJson = readStream.ReadToEnd();

                    List<CarDto> objResponse = (List<CarDto>)js.Deserialize(strJson, typeof(List<CarDto>));

                    //var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    //Stream stream = httpResponseItem.GetResponseStream();

                    //DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<CarDto>));
                    //List<CarDto> objResponse = (List<CarDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        paging.PageNo = Convert.ToInt32(pageNo);
                        paging.PageLength = Convert.ToInt32(pageLength);                       


                        if (objResponse.Count() > 0)
                        {
                            totalRecord = objResponse.Count();

                            totalPage = Math.Ceiling(totalRecord / pageLength);

                            var outResponse = objResponse.Skip(Convert.ToInt32(pageLength * (pageNo - 1))).Take(Convert.ToInt32(pageLength));

                            foreach (var r in outResponse)
                            {
                                if (r.enter_image_path_file != null && r.image_url != null)
                                {
                                    if (r.enter_image_path_file.Length > 0 && r.image_url.Length > 0)
                                    {
                                        r.enter_image = DownloadImage(r.image_url + r.enter_image_path_file);
                                    }
                                }
                            }

                            paging.TotalRecord = Convert.ToInt32(totalRecord);
                            paging.TotalPage  = Convert.ToInt32(totalPage);                            
                            paging.Data = outResponse;
                        }
                        
                        result.Status = true;
                        result.Data = paging;
                    } else
                    {
                        result.Status = true;
                        result.Message = String.Empty;
                        result.Data = null;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListDiscountOwner/{User_id}/{Has_Receipt}")]
        public IHttpActionResult ListDiscountOwner(string User_id,string Has_Receipt)
        {
            string serviceUrl = "CarparkDiscount/ListDiscountOwner/{User_id}/{Has_Receipt}";

            var values = ControllerContext.Request.GetQueryNameValuePairs();

            var keyPageNo = values.LastOrDefault(x => x.Key == "pageno").Value;
            var keyPageLength = values.LastOrDefault(x => x.Key == "pagelength").Value;

            decimal pageNo = 1;
            decimal pageLength = 10;
            decimal totalPage = 1;
            decimal totalRecord = 0;

            if (keyPageNo != null)
            {
                pageNo = Convert.ToInt32(keyPageNo);
            }

            if (keyPageLength != null)
            {
                pageLength = Convert.ToInt32(keyPageLength);
            }


            serviceUrl = serviceUrl.Replace("{User_id}", User_id);
            serviceUrl = serviceUrl.Replace("{Has_Receipt}", Has_Receipt);

            var result = new ResultDto();
            var paging = new PagingDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    js.MaxJsonLength = int.MaxValue;
                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);

                    string strJson = readStream.ReadToEnd();

                    List<CarDto> objResponse = (List<CarDto>)js.Deserialize(strJson, typeof(List<CarDto>));
                    //DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<CarDto>));
                    //List<CarDto> objResponse = (List<CarDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        paging.PageNo = Convert.ToInt32(pageNo);
                        paging.PageLength = Convert.ToInt32(pageLength);


                        if (objResponse.Count() > 0)
                        {
                            totalRecord = objResponse.Count();

                            totalPage = Math.Ceiling(totalRecord / pageLength);

                            var outResponse = objResponse.Skip(Convert.ToInt32(pageLength * (pageNo - 1))).Take(Convert.ToInt32(pageLength));

                            foreach (var r in outResponse)
                            {
                                if (r.enter_image_path_file != null && r.image_url != null)
                                {
                                    if (r.enter_image_path_file.Length > 0 && r.image_url.Length > 0)
                                    {
                                        r.enter_image = DownloadImage(r.image_url + r.enter_image_path_file);
                                    }
                                }
                            }

                            paging.TotalRecord = Convert.ToInt32(totalRecord);
                            paging.TotalPage = Convert.ToInt32(totalPage);
                            paging.Data = outResponse;
                        }

                        result.Status = true;
                        result.Data = paging;
                    }
                    else
                    {
                        result.Status = true;
                        result.Message = String.Empty;
                        result.Data = null;
                    }
                }
            }

            return Json(result);
        }

        private String DownloadImage (string url)
        {
            byte[] resizedImage;
            try
            {
                byte[] image = new WebClient().DownloadData(url);

                using (var ms = new MemoryStream(image))
                {
                    var orginalImage = Image.FromStream(ms);

                    ImageFormat orginalImageFormat = orginalImage.RawFormat;
                    int orginalImageWidth = orginalImage.Width;
                    int orginalImageHeight = orginalImage.Height;
                    int resizedImageWidth = orginalImageWidth / 2;
                    int resizedImageHeight = orginalImageHeight / 2;
                    using (Bitmap bitmapResized = new Bitmap(orginalImage, resizedImageWidth, resizedImageHeight))
                    {
                        using (MemoryStream streamResized = new MemoryStream())
                        {
                            bitmapResized.Save(streamResized, orginalImageFormat);
                            resizedImage = streamResized.ToArray();
                        }
                    }

                }

                return Convert.ToBase64String(resizedImage);

            } catch (Exception)
            {
                return null;
            }
            
        }

        private ResultDto GetDiscount(string Url,string Emp_id)
        {
            string serviceUrl = "CarparkDiscount/GetPersonDiscount/{emp_id}";

            serviceUrl = serviceUrl.Replace("{emp_id}", Emp_id);

            var result = new ResultDto();

            var strUrl = Url;

            if (!strUrl.Substring(strUrl.Length).Equals("/"))
            {
                strUrl = strUrl + "/" + serviceUrl;
            }

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(strUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Accept = "application/json";
                httpWebRequest.Method = "GET";

                var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                Stream stream = httpResponseItem.GetResponseStream();

                DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(DiscountDto));
                DiscountDto objResponse = (DiscountDto)dataContractJsonSerializer.ReadObject(stream);
                if (objResponse != null)
                {
                    result.Status = true;
                    result.Data = objResponse;
                }
                else
                {
                    result.Status = true;
                    result.Message = String.Empty;
                    result.Data = null;
                }
            }
            catch (Exception ex)
            {

                result.Status = false;
                result.Message = ex.Message;
            }
                   

            return result;
        }

        [HttpGet]
        [Route("api/Carpark/GetDiscount/{Emp_id}")]
        public IHttpActionResult GetDiscount(string Emp_id)
        {
            string serviceUrl = "CarparkDiscount/GetPersonDiscount/{emp_id}";

            serviceUrl = serviceUrl.Replace("{emp_id}", Emp_id);

            var result = new ResultDto();
            string strLoginId = String.Empty;            
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {
                    result = GetDiscount(url, Emp_id);
                }                    
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListDiscountReason")]
        public IHttpActionResult ListDiscountReason()
        {

            string serviceUrl = "DiscountReason";
            
            var result = new ResultDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<DiscountReasonDto>));
                    
                    List<DiscountReasonDto> objResponse = (List<DiscountReasonDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {                        
                        result.Status = true;
                        result.Data = objResponse;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListDiscountGroup")]
        public IHttpActionResult ListDiscountGroup()
        {

            string serviceUrl = "Discount";

            var result = new ResultDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<DiscountGroupDto>));

                    List<DiscountGroupDto> objResponse = (List<DiscountGroupDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListDiscountBooth/{Booth_id}")]
        public IHttpActionResult ListDiscountBooth(string Booth_id)
        {
            
            string serviceUrl = "BoothDiscount?p_booth_id={p_booth_id}";
            serviceUrl = serviceUrl.Replace("{p_booth_id}", Booth_id);

            var result = new ResultDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<DiscountBoothDto>));

                    List<DiscountBoothDto> objResponse = (List<DiscountBoothDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListDiscountReceipt/{Amt}")]
        public IHttpActionResult ListDiscountReceipt(string Amt)
        {

            string serviceUrl = "DiscountReceipt?p_amt={p_amt}";
            serviceUrl = serviceUrl.Replace("{p_amt}", Amt);

            var result = new ResultDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<DiscountDto>));

                    List<DiscountDto> objResponse = (List<DiscountDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                }
            }

            return Json(result);
        }

        [HttpGet]
        [Route("api/Carpark/ListCarDiscountReceipt/{Seq_id}")]
        public IHttpActionResult ListCarDiscountReceipt(string Seq_id)
        {
            NameValueCollection qscoll = Request.RequestUri.ParseQueryString();
            string Discount_id = String.Empty;            

            if (qscoll["discount_id"] != null)
            {
                Discount_id = qscoll["discount_id"];
            }
           

            string serviceUrl = "CarparkDiscountReceipt/{p_seq_id}?discountId={p_discount_id}";
            serviceUrl = serviceUrl.Replace("{p_seq_id}", Seq_id);
            serviceUrl = serviceUrl.Replace("{p_discount_id}", Discount_id);

            var result = new ResultDto();
            string strLoginId = String.Empty;
            string strIpAddress = string.Empty;
            result = this.HasAuthorize(ref strLoginId, ref strIpAddress);
            if (result.Status)
            {
                result = new ResultDto();
                string url = String.Empty;
                if ((url = GetUrl(serviceUrl)) != String.Empty)
                {

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Accept = "application/json";
                    httpWebRequest.Method = "GET";

                    var httpResponseItem = (HttpWebResponse)httpWebRequest.GetResponse();
                    Stream stream = httpResponseItem.GetResponseStream();

                    DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(List<DiscountReceiptDto>));

                    List<DiscountReceiptDto> objResponse = (List<DiscountReceiptDto>)dataContractJsonSerializer.ReadObject(stream);
                    if (objResponse != null)
                    {
                        result.Status = true;
                        result.Data = objResponse;
                    }
                }
            }

            return Json(result);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountReasonDto
    {
        public string reason_cd { get; set; }
        public string reason_desc { get; set; }        
    }
}
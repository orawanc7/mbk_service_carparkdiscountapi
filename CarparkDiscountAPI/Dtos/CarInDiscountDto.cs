﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class CarInDiscountDto
    {
        public string discount_id { get; set; }
        public string discount_desc { get; set; }
        public DateTime? discount_time { get; set; }
        public string discount_time_text { get; set; }
        public string discount_reason_cd { get; set; }
        public string discount_reason_desc { get; set; }
        public string creation_by { get; set; }
        public string discount_type { get; set; }
        public string lock_flag { get; set; }
        public string public_flag { get; set; }
    }
}
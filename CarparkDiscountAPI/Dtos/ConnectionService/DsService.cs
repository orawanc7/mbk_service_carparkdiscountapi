﻿using CarparkDiscountAPI.Dtos.LoginService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.ConnnectionService
{
    [XmlRoot(ElementName = "dsService")]
    public class DsService
    {
        [XmlElement(ElementName = "Error")]
        public Error Error { get; set; }

        [XmlElement(ElementName = "DBConnection")]
        public List<DBConnection> DBConnections { get; set; }
    }    
}
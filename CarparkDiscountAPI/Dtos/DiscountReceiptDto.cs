﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountReceiptDto
    {
        public Decimal? seq_id { get; set; }
        public string discount_id { get; set; }
        public string discount_group_desc { get; set; }
        public Decimal? receipt_seq_id { get; set; }
        public string discount_group_id { get; set; }
        public string discount_group_name { get; set; }
        public string receipt_no { get; set; }
        public Decimal? receipt_amt { get; set; }
    }
}
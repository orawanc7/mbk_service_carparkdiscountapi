﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class PagingDto
    {
        public int TotalPage { get; set; }
        public int TotalRecord { get; set; }
        public int PageNo { get; set;  }
        public int PageLength { get; set; }
        public object Data { get; set; }
    }
}
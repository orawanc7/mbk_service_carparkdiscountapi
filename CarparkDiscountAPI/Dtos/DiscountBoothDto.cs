﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarparkDiscountAPI.Dtos
{
    public class DiscountBoothDto
    {
        public Decimal? booth_id { get; set; }
        public string booth_desc { get; set; }
        public string discount_id { get; set; }

        public string discount_desc { get; set; }
        public string discount_group_id { get; set; }

        public Decimal? discount_click { get; set; }

        public Decimal? hotkey_id { get; set; }

        public string condition_flag { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CarparkDiscountAPI.Dtos.LoginService
{
    public class LoginInfo
    {
        [XmlElement(ElementName = "LoginId")]
        public string LoginId { get; set; }

        [XmlElement(ElementName = "User_ID")]
        public string UserId { get; set; }
        
        [XmlElement(ElementName = "User_LoginID")]
        public string UserLoginId { get; set; }

        [XmlElement(ElementName = "User_EmployeeCode")]
        public string EmployeeCode { get; set; }

        [XmlElement(ElementName = "User_DomainName")]
        public string DomainName { get; set; }

        [XmlElement(ElementName = "User_NameTH")]
        public string UserNameTh { get; set; }

        [XmlElement(ElementName = "User_NameEN")]
        public string UserNameEn { get; set; }

        [XmlElement(ElementName = "User_Language")]
        public string Language { get; set; }

        [XmlElement(ElementName = "User_CompanyCode")]
        public string CompanyCode { get; set; }

        [XmlElement(ElementName = "User_CompanyNameTH")]
        public string CompanyNameTh { get; set; }

        [XmlElement(ElementName = "User_CompanyNameEN")]
        public string CompanyNameEn { get; set; }

        [XmlElement(ElementName = "Branch_Code")]
        public string BranchCode { get; set; }

        [XmlElement(ElementName = "Branch_NameTH")]
        public string BranchNameTh { get; set; }

        [XmlElement(ElementName = "Branch_NameEN")]
        public string BranchNameEn { get; set; }

        [XmlElement(ElementName = "Dept_Code")]
        public string DeptCode { get; set; }

        [XmlElement(ElementName = "Dept_NameTH")]
        public string DeptNameTh { get; set; }

        [XmlElement(ElementName = "Dept_NameEN")]
        public string DeptNameEn { get; set; }

        [XmlElement(ElementName = "ipAddress")]
        public string IpAddress { get; set; }
    }
}                